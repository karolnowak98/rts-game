using RTS.Input;
using RTS.Selection;
using RTS.Units.Enemy;
using UnityEngine;
using Zenject;

namespace RTS.Cursors
{
    public class CursorController : MonoBehaviour
    {
        [SerializeField] private Texture2D cursorStandard;
        [SerializeField] private Texture2D cursorGathering;
        [SerializeField] private Texture2D cursorAttack;

        private SelectionController selectionController;
        private InputController inputController;

        private bool isStandardCursor = true;
        private bool isGatheringCursor = false;
        private bool isAttackCursor = false;

        [Inject]
        private void Construct(SelectionController selectionController,
                               InputController inputController)
        {
            this.selectionController = selectionController;
            this.inputController = inputController;
        }

        private void Start()
        {
            SetStandard();
        }

        private void Update()
        {
            if(Utils.IsMouseOverUI)
            {
                return;
            }

            switch (inputController.hitLayer)
            {
                case 30:
                    if (!isGatheringCursor)
                    {
                        if (selectionController.HasSelectedWorker)
                        {
                            SetGathering();
                        }
                    }
                    break;
                case 10:
                    if (!isAttackCursor)
                    {
                        if (selectionController.HasSelectedUnit)
                        {
                            if (inputController.hit.transform.GetComponent<EnemyUnit>().isVisible)
                            {
                                SetAttack();
                            }
                        }
                    }
                    break;
                default:
                    if (!isStandardCursor)
                    {
                        SetStandard();
                    }
                    break;
            }
        }

        private void SetStandard()
        {
            Cursor.SetCursor(cursorStandard, Vector2.zero, CursorMode.Auto);
            isStandardCursor = true;
            isGatheringCursor = false;
        }

        private void SetGathering()
        {
            Cursor.SetCursor(cursorGathering, Vector2.zero, CursorMode.Auto);
            isStandardCursor = false;
            isGatheringCursor = true;
        }

        private void SetAttack()
        {
            Cursor.SetCursor(cursorAttack, Vector2.zero, CursorMode.Auto);
            isStandardCursor = false;
            isGatheringCursor = true;
        }
    }
}