using RTS.Buildings.Base;
using RTS.Input;
using RTS.Input.Base;
using RTS.Other;
using RTS.Other.Base;
using RTS.Selection;
using UnityEngine;
using Zenject;

namespace RTS.Buildings.Player
{
    public class PlayerBuilding : Building, ISelectionable, IMouseResponsive, IKillable
    {
        [SerializeField] private Highlight highlight;
        [SerializeField] private Transform flag;

        private SelectionController selectionController;
        private InputController inputController;
        private PlayerBuildingsController buildingsController;

        public bool isSelected = false;

        [Inject]
        public void Construct(PlayerBuildingsController buildingsController,
                              InputController inputController,
                              SelectionController selectionController)
        {
            this.buildingsController = buildingsController;
            this.inputController = inputController;
            this.selectionController = selectionController;
        }

        protected override void Start()
        {
            base.Start();
            transform.SetParent(buildingsController.GetParent(attributes.buildingType));
        }
        
        protected override void Update()
        {
            base.Update();
        }

        private void OnEnable()
        {
            HealthBelowZero += Kill;
        }

        private void OnDisable()
        {
            HealthBelowZero -= Kill;
        }

        public void Select()
        {
            isSelected = true;
            highlight.SetAlpha(1f);
            highlight.Show();
        }

        public void Deselect()
        {
            isSelected = false;
            highlight.Hide();
            highlight.SetAlpha(0.5f);
        }
        
        public void MouseOver()
        {
            highlight.Show();
            healthBar.Show();
        }

        public void MouseExit()
        {
            if (!isSelected)
            {
                highlight.Hide();
            }
            healthBar.Hide();
        }

        public void Kill() 
        {
            Destroy(gameObject);
        }

        public void SetFlagPosition(Vector3 position)
        {
            Vector3 calculatedPosition = position;
            calculatedPosition.y = Utils.GetHeightOfTerrain(position) + 0.5f;
            flag.position = calculatedPosition;
        }
    }
}