using RTS.Buildings.Player;
using RTS.Resources;
using RTS.Units.Base;
using RTS.Units.Player;
using UnityEngine;
using Zenject;

public class RecruitBuilding : MonoBehaviour
{
    [SerializeField] private MeshRenderer destinationPoint;
    [SerializeField] private Transform spawnPoint;
    [SerializeField] private PlayerBuilding building;

    private ResourcesController resourcesController;
    private PlayerUnit.Factory unitFactory;

    [Inject]
    public void Construct(ResourcesController resourcesController,
                          PlayerUnit.Factory unitFactory)
    {
        this.resourcesController = resourcesController;
        this.unitFactory = unitFactory;
    }

    private void Update()
    {
        HandleFlagVisibility();
    }

    private void HandleFlagVisibility()
    {
        if (building.isSelected)
        {
            destinationPoint.enabled = true;
        }

        else
        {
            destinationPoint.enabled = false;
        }
    }

    public void RecruitUnit(UnitSO unitType)
    {
        if (resourcesController.AreEnoughResources(unitType))
        {
            GameObject unit = unitFactory.Create(unitType.prefab).gameObject;
            unit.GetComponent<PlayerUnit>().SetPosition(spawnPoint.position);
            //unit.GetComponent<IMovable>().MoveUnit(destinationPoint.position);
            resourcesController.ConsumeResources(unitType.woodCost, unitType.stoneCost);
        }
    }
}