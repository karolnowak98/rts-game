using UnityEngine;
using Zenject;
using RTS.Selection;
using UnityEngine.InputSystem;
using RTS.Input;
using RTS.Buildings.Player;
using RTS.Buildings.Base;

namespace RTS.Buildings
{
    public class PlayerBuildingsController : MonoBehaviour
    {
        [SerializeField] private Transform Main;
        [SerializeField] private Transform Military;
        [SerializeField] private Transform Watchtowers;

        private SelectionController selectionController;
        private InputController inputController;
        private PlayerInput playerInput;

        [Inject]
        public void Construct(SelectionController selectionController,
                              InputController inputController,
                              PlayerInput playerInput)
        {
            this.selectionController = selectionController;
            this.inputController = inputController;
            this.playerInput = playerInput;
        }

        private void OnEnable()
        {
            playerInput.actions["RMB"].performed += SetActionForBuildings;
        }

        private void OnDisable()
        {
            if(playerInput != null)
            {
                playerInput.actions["RMB"].performed -= SetActionForBuildings;
            }
        }

        public Transform GetParent(BuildingSO.BuildingType buildingType)
        {
            return buildingType switch
            {
                BuildingSO.BuildingType.Main => Main,
                BuildingSO.BuildingType.Military => Military,
                BuildingSO.BuildingType.Watchtower => Watchtowers,
                _ => null,
            };
        }

        public void SetActionForBuildings(InputAction.CallbackContext ctx)
        {
            if(Utils.IsMouseOverUI) return;
            if(!selectionController.HasSelectedBuilding) return;
            
            foreach (Transform building in selectionController.SelectedBuildings)
            {
                PlayerBuilding playerBuilding = building.GetComponent<PlayerBuilding>();
                playerBuilding.SetFlagPosition(inputController.hit.point);
            }
        }
    }
}
