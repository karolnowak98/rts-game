using RTS.Other;
using RTS.Other.Base;
using UnityEngine;
using UnityEngine.InputSystem;

namespace RTS.Buildings.Base
{
    public class Building : MonoBehaviour, IDamageable
    {
        [SerializeField] protected HealthBar healthBar;

        protected delegate void HealthChangedHandler();
        protected delegate void HealthBelowZeroHandler();
        protected event HealthChangedHandler HealthChanged;
        protected event HealthBelowZeroHandler HealthBelowZero;

        protected int health;
        protected int Health
            {
                get => Health;
                set
                {
                    int previousHealth = health;

                    if (previousHealth != value)
                    {
                        health = value;
                        HealthChanged?.Invoke();
                    }

                    if (health <= 0)
                    {
                        HealthBelowZero?.Invoke();
                    }
                }
            }
        protected int armor;

        private Camera cam;
        private PlayerInput playerInput;

        public BuildingSO attributes;

        protected virtual void Start()
        {
            armor = attributes.armor;
            Health = attributes.health;
            cam = Camera.main;
        }

        protected virtual void Update()
        {
            healthBar.FaceHealthBarToCamera(cam);
        }

        public void TakeDamage(int damage) 
        {
            int updatedDamage = damage - attributes.armor;

            Health -= updatedDamage;
        }
    }
}