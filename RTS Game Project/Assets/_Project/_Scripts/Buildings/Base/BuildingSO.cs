using UnityEngine;

namespace RTS.Buildings.Base
{
    [CreateAssetMenu(fileName = "New Building", menuName = "New Building")]
    public class BuildingSO : ScriptableObject
    {
        public enum BuildingType
        {
            Main,
            Military,
            Watchtower
        }

        [Space(15)]
        [Header("Building Settings")]

        public new string name;
        public Sprite icon;
        public GameObject prefab;
        public BuildingType buildingType;
        public bool isPlayerBuilding;

        [Space(15)]
        [Header("Building Base Stats")]
        [Space(40)]

        public int health;
        public int armor;
        public float fov;
        public float nightFov;
    }
}