using System.Collections.Generic;
using UnityEngine;
using Zenject;
using RTS.Selection.Signals;
using RTS.Input;
using UnityEngine.InputSystem;

namespace RTS.Selection
{
    public class SelectionController : MonoBehaviour
    {
        [SerializeField] private Transform playerUnits;
        [SerializeField] private Transform playerBuildings;

        private InputController inputController;
        private SignalBus signalBus;
        private PlayerInput playerInput;
        private Rect rect;
        private Bounds bounds;
        private Vector2 startMousePos;
        private Camera mainCam;
        private ISelectionable selectionable;
        private bool isDraging = false;
        private bool isShiftClicked = false;

        private List<Transform> selectedUnits;
        private List<Transform> selectedBuildings;
        private List<Transform> selectedWorkers;
        private List<Transform> selectedMainBuildings;
        private List<Transform> selectedMilitaryBuildings;
        private List<Transform> selectedWatchtowerBuildings;

        public IEnumerable<Transform> SelectedUnits => selectedUnits;
        public IEnumerable<Transform> SelectedWorkers => selectedWorkers;

        public IEnumerable<Transform> SelectedBuildings => selectedBuildings;
        public IEnumerable<Transform> SelectedMainBuildings => selectedMainBuildings;
        public IEnumerable<Transform> SelectedMilitaryBuildings => selectedMilitaryBuildings;
        public IEnumerable<Transform> SelectedWatchtowerBuildings => selectedWatchtowerBuildings;

        public int SelectedUnitsNumber => selectedUnits.Count;
        public int SelectedBuildingsNumber => selectedBuildings.Count;
        public int SelectedWorkersNumber => selectedWorkers.Count;
        public int SelectedMainBuildingsNumber => selectedMainBuildings.Count;
        public int SelectedMilitaryBuildingsNumber => selectedMilitaryBuildings.Count;
        public int SelectedWatchtowerBuildingsNumber => selectedWatchtowerBuildings.Count;

        public bool HasSelectedAnything => HasSelectedBuilding && HasSelectedUnit;
        public bool HasSelectedUnit => SelectedUnitsNumber > 0;
        public bool HasSelectedWorker => SelectedWorkersNumber > 0;
        public bool HasSelectedBuilding => SelectedBuildingsNumber > 0;
        public bool HasSelectedMain => SelectedMainBuildingsNumber > 0;
        public bool HasSelectedMilitary => SelectedMilitaryBuildingsNumber > 0;
        public bool HasSelectedWatchtower => SelectedWatchtowerBuildingsNumber > 0;

        [Inject]
        public void Construct(InputController inputController,
                              SignalBus signalBus, 
                              PlayerInput playerInput)
        {
            this.inputController = inputController;
            this.signalBus = signalBus;
            this.playerInput = playerInput;
        }

        private void Start()
        {
            mainCam = Camera.main;
            selectedUnits = new List<Transform>();
            selectedWorkers = new List<Transform>();
            selectedBuildings = new List<Transform>();
            selectedMainBuildings = new List<Transform>();
            selectedMilitaryBuildings = new List<Transform>();
            selectedWatchtowerBuildings = new List<Transform>();
        }
        
        private void OnEnable()
        {
            playerInput.actions["LeftShift"].performed += _ => isShiftClicked = true;
            playerInput.actions["LeftShift"].canceled += _ => isShiftClicked = false;

            playerInput.actions["LMB"].performed +=  SelectLMB;
            playerInput.actions["LMB"].canceled += ReleaseLMB;
        }

        private void OnDisable()
        {
            playerInput.actions["LeftShift"].performed -= _ => isShiftClicked = true;
            playerInput.actions["LeftShift"].canceled -= _ => isShiftClicked = false;
            
            playerInput.actions["LMB"].performed -=  SelectLMB;
            playerInput.actions["LMB"].canceled -= ReleaseLMB;
        }

        private void OnGUI()
        {
            if (isDraging)
            {
                DrawSelectionRect();
            }
        }

        private bool IsWithinSelectionBounds(Transform transform)
        {
            if (!isDraging)
            {
                return false;
            }
            bounds = MultiSelect.GetVPBounds(mainCam, startMousePos, Utils.MousePos());
            return bounds.Contains(mainCam.WorldToViewportPoint(transform.position));
        }

        private void DrawSelectionRect()
        {
            rect = MultiSelect.GetScreenRect(startMousePos, Utils.MousePos());
            MultiSelect.DrawScreenRect(rect, new Color(0f, 0f, 0f, 0.25f));
            MultiSelect.DrawScreenRectBorder(rect, 3, Color.green);
        }

        private void Select(List<Transform> selected, Transform obj)
        {
            selectionable = obj.GetComponent<ISelectionable>();
            
            if (selectionable != null)
            {
                selectionable.Select();
            }
            selected.Add(obj);

            switch(obj.parent.name)
            {
                case "Main":
                    selectedMainBuildings.Add(obj);
                    break;
                case "Military":
                    selectedMilitaryBuildings.Add(obj);
                    break;
                case "Watchtowers":
                    selectedWatchtowerBuildings.Add(obj);
                    break;
                default:
                break;
            }

            signalBus.Fire<SelectionChangedSignal>();
        }     

        private void Deselect(List<Transform> selected, int i)
        {
            selectionable = selected[i].GetComponent<ISelectionable>();

            if (selectionable != null)
            {
                selectionable.Deselect();
            }
            selected.RemoveAt(i);

            signalBus.Fire<SelectionChangedSignal>();
        }

        private void DeselectAll(List<Transform> selected)
        {
            for (int i = selected.Count - 1; i >= 0; i--)
            {
                Deselect(selected, i);
            }
        }

        private void DeselectAllUnits()
        {
            for(int i = SelectedUnitsNumber - 1; i >=0; i--)
            {
                Deselect(selectedUnits, i);
            }
        }

        private void DeselectAllBuildings()
        {
            for(int i = SelectedBuildingsNumber - 1; i >=0; i--)
            {
                Deselect(selectedBuildings, i);
            }
            selectedMainBuildings.Clear();
            selectedWatchtowerBuildings.Clear();
            selectedMilitaryBuildings.Clear();
        }

        private void SelectUnit(Transform unit, bool canMultiSelect = false)
        {
            if (!canMultiSelect)
            {
                DeselectAllUnits();
            }

            Transform findUnit = selectedUnits.Find(unit.Equals);
            
            if(findUnit != null)
            {
                Deselect(selectedUnits, selectedUnits.IndexOf(findUnit));
            }

            else
            {
                Select(selectedUnits, unit);
            }
        }

        private void SelectBuilding(Transform building, bool canMultiSelect = false)
        {
            if (!canMultiSelect)
            {
                DeselectAll(selectedBuildings);
            }

            Transform findBuilding = selectedBuildings.Find(building.Equals);

            if(findBuilding != null)
            {
                Deselect(selectedBuildings, selectedUnits.IndexOf(findBuilding));
            }

            else
            {
                switch (building.parent.name)
                {
                    case "Main":
                        if (HasSelectedMain)
                        {
                            Select(selectedBuildings, building);
                        }

                        else
                        {
                            DeselectAllBuildings();
                            Select(selectedBuildings, building);
                        }
                        break;

                    case "Military":
                        if (HasSelectedMilitary)
                        {
                            Select(selectedBuildings, building);
                        }

                        else
                        {
                            DeselectAllBuildings();
                            Select(selectedBuildings, building);
                        }
                        break;

                    case "Watchtowers":
                        if (HasSelectedWatchtower)
                        {
                            Select(selectedBuildings, building);
                        }

                        else
                        {
                            DeselectAllBuildings();
                            Select(selectedBuildings, building);
                        }
                        break;
                }
            }
        }

        private void SelectLMB(InputAction.CallbackContext context)
        {
            startMousePos = Utils.MousePos();
            switch (inputController.hitLayer.value)
            {
                case 8:
                    SelectUnit(inputController.hit.transform, isShiftClicked);
                    DeselectAllBuildings();
                    break;

                case 9:
                    SelectBuilding(inputController.hit.transform, isShiftClicked);
                    DeselectAllUnits();
                    break;

                default:
                    isDraging = true;
                    DeselectAllUnits();
                    DeselectAllBuildings();
                    break;
            }
        }

        private void ReleaseLMB(InputAction.CallbackContext context)
        {
            foreach (Transform type in playerUnits)
            {
                foreach (Transform unit in type)
                {
                    if (IsWithinSelectionBounds(unit))
                    {
                        SelectUnit(unit, true);
                    }
                }
            }

            if(!HasSelectedUnit)
            {
                foreach (Transform type in playerBuildings)
                {
                    foreach (Transform building in type)
                    {
                        if(!IsWithinSelectionBounds(building))
                        {
                            break;
                        }

                        if (type.name == "Main")
                        {
                            Select(selectedBuildings, building);
                        }

                        else if (!HasSelectedMain)
                        {
                            if (type.name == "Military")
                            {
                                Select(selectedBuildings, building);
                            }

                            else if (!HasSelectedMilitary)
                            {
                                if (type.name == "Watchtowers")
                                {
                                    Select(selectedBuildings, building);
                                }
                            }
                        }
                    }
                }
            }
            signalBus.Fire<SelectionChangedSignal>();
            isDraging = false;
        }
    }
}