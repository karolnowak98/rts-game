using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace RTS.Other
{
    public class HealthBar : MonoBehaviour, IShowable
    {
        private PlayerInput playerInput;

        [Inject]
        private void Construct(PlayerInput playerInput)
        {
            this.playerInput = playerInput;
        }

        private void OnEnable()
        {
            playerInput.actions["LeftAlt"].performed += _ => Show();
            playerInput.actions["LeftAlt"].canceled += _ => Hide();
        }

        private void OnDisable()
        {
            playerInput.actions["LeftAlt"].performed -= _ => Show();
            playerInput.actions["LeftAlt"].canceled -= _ => Hide();
        }

        private void Start()
        {
            Hide();
        }

        public void FaceHealthBarToCamera(Camera cam)
        {
            transform.LookAt(transform.position +
                cam.transform.rotation * Vector3.forward,
                cam.transform.rotation * Vector3.up);
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}