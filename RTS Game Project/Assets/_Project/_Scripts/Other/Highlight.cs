using UnityEngine;

namespace RTS.Other
{
    public class Highlight : MonoBehaviour, IShowable
    {
        [SerializeField] private SpriteRenderer spriteRenderer;

        private Color color;

        public void SetAlpha(float alphaOfRenderer)
        {
            color = spriteRenderer.color;
            color.a = alphaOfRenderer;
            spriteRenderer.color = color;
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }
    }
}