namespace RTS.Other.Base
{
    public interface IDamageable
    {
        void TakeDamage(int damage);
    }
}