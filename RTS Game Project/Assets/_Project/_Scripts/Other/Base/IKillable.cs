namespace RTS.Other.Base
{
    public interface IKillable
    {
        void Kill();
    }
}