using RTS.Game.Lighting.Signals;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace RTS.Game.Lighting
{
    [ExecuteAlways]
    public class LightingController : MonoBehaviour
    {
        [SerializeField] private Light directionalLight;
        [SerializeField] private LightingPreset preset;
        [SerializeField, Range(0, 24)] private float timeOfDay;
        [SerializeField] private float timeDecreaser = 35f;
        //timeDecreaser * 24 wychodzi suma sekund ktora mija przez caly dzien w grze

        private SignalBus signalBus;
        private bool isDay = true;
        private Vector3 sunPosition;
        private Light[] lights;

        [Inject]
        private void Construct(SignalBus signalBus)
        {
            this.signalBus = signalBus;
        }

        private void Update()
        {
            HandleDayState();

            if (preset == null)
            {
                return;
            }

            if (Application.isPlaying)
            {
                timeOfDay += Time.deltaTime / timeDecreaser;
                timeOfDay %= 24;
                UpdateLighting(timeOfDay / 24f);
            }

            else
            {
                UpdateLighting(timeOfDay / 24f);
            } 
        }

        private void HandleDayState()
        {
            if (isDay)
            {
                if ((timeOfDay > 18) || (timeOfDay < 7))
                {
                    isDay = false;
                    signalBus.Fire<NightComesSignal>();
                }
            }

            else
            {
                if ((timeOfDay < 18) && (timeOfDay > 7))
                {
                    isDay = true;
                    signalBus.Fire<DayComesSignal>();
                }
            }
        }

        private void UpdateLighting(float timePercent)
        {
            RenderSettings.ambientLight = preset.ambientColor.Evaluate(timePercent);
            RenderSettings.fogColor = preset.fogColor.Evaluate(timePercent);
            if (directionalLight != null)
            {
                directionalLight.color = preset.directionalColor.Evaluate(timePercent);
                sunPosition = new Vector3((timePercent * 360f) - 90f, 170f, 0);
                directionalLight.transform.localRotation = Quaternion.Euler(sunPosition);
            }
        }

        private void OnValidate()
        {
            if (directionalLight != null)
            {
                return;
            }
            
            if (RenderSettings.sun != null)
            {
                directionalLight = RenderSettings.sun;
            }

            else
            {
                lights = GameObject.FindObjectsOfType<Light>();
                foreach (Light light in lights)
                {
                    if (light.type == LightType.Directional)
                    {
                        directionalLight = light;
                        return;
                    }
                }
            }
        }
    }
}