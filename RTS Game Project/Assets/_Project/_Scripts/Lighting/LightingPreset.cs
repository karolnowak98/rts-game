using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RTS.Game.Lighting
{
    [Serializable]
    [CreateAssetMenu(fileName = "Lighting Preset", menuName = "Scriptables/Lighting Preset", order = 1)]
    public class LightingPreset : ScriptableObject
    {
        public Gradient ambientColor;
        public Gradient directionalColor;
        public Gradient fogColor;
    }
}