using RTS.Selection;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;
using RTS.Buildings;
using RTS.UI.Views.Signals;
using RTS.Input.Base;
using RTS.States;
using RTS.Units.Player;

namespace RTS.Input
{
    public class InputController : MonoBehaviour
    {
        private SignalBus signalBus;
        private PlayerInput playerInput;
        private StateController stateController;
        private SelectionController selectionController;
        private PlayerUnitsController playerUnitsController;
        private PlayerBuildingsController buildingsController;
        private Ray ray;
        private Camera mainCam;
        private GameObject hitObject;
        private GameObject lastHitObject;
        private ISelectionable selectionable;
        private IMouseResponsive responsive;

        public LayerMask hitLayer;
        public RaycastHit hit;
        public bool leftAltClicked = false;

        [Inject]
        public void Construct(SignalBus signalBus,
                              StateController stateController,
                              SelectionController selectionController, 
                              PlayerUnitsController playerUnitsController,
                              PlayerBuildingsController buildingsController)
        {
            this.signalBus = signalBus;
            this.stateController = stateController;
            this.selectionController = selectionController;
            this.playerUnitsController = playerUnitsController;
            this.buildingsController = buildingsController;
        }

        private void Start() 
        {
            playerInput = GetComponent<PlayerInput>();
            mainCam = Camera.main;
        }

        private void Update()
        {
            ControlRaycast();
        }

        private void OnEnable()
        {
            //playerInput.actions["LBM"].performed +=
        }

        private void OnDisable()
        {
            
        }

        private void ControlRaycast()
        {
            ray = mainCam.ScreenPointToRay(Utils.MousePos());
            if (Physics.Raycast(ray, out hit))
            {
                if (!Utils.IsMouseOverUI)
                {
                    if(lastHitObject != null)
                    {
                        responsive = lastHitObject.GetComponent<IMouseResponsive>();
                        if (responsive != null)
                        {
                            responsive.MouseExit();
                        }
                    }

                    hitObject = hit.transform.gameObject;
                    hitLayer = hitObject.layer;
                    responsive = hitObject.GetComponent<IMouseResponsive>();

                    if (responsive != null)
                    {
                        responsive.MouseOver();
                    }
                }
            }
            lastHitObject = hitObject;
        }

        public void PButtonClick(InputAction.CallbackContext ctx)
        {
            bool PButtonClicked = ctx.ReadValueAsButton();

            if(PButtonClicked && ctx.performed)
            {
                signalBus.Fire<PauseUnpauseGameSignal>();
            }
        }

        public void EscapeClick(InputAction.CallbackContext ctx)
        {
            bool escapeClicked = ctx.ReadValueAsButton();

            if (escapeClicked && ctx.performed)
            {
                if (stateController.IsActiveState())
                {
                    signalBus.Fire<ShowGameMenuSignal>();
                    stateController.ChangeStateToEsc();                   
                }
            }
        }
    }
}