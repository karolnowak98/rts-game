namespace RTS.Input.Base
{
    public interface IMouseResponsive
    {
        void MouseOver();
        void MouseExit();
    }
}