using RTS.States.Base;
using UnityEngine;

namespace RTS.States
{
    public class EscState : IState
    {
        public void Enter()
        {
            Debug.Log("GAME STATE CHANGED - Esc");
            Time.timeScale = 0;
        }

        public void Exit()
        {

        }

        public void FixedTick()
        {
            //
        }

        public void Tick()
        {
            //
        }
    }
}
