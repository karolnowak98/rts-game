using RTS.States.Base;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RTS.States
{
    public class MainMenuState : IState
    {
        public void Enter()
        {
            Debug.Log("GAME STATE CHANGED - MainMenu");
            SceneManager.LoadScene("MainMenu");
        }

        public void Exit()
        {
            //
        }

        public void FixedTick()
        {
            //
        }

        public void Tick()
        {
            //
        }
    }
}
