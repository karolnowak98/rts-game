using RTS.States.Base;
using UnityEngine;

namespace RTS.States
{
    public class EndState : IState
    {
        public void Enter()
        {
            Debug.Log("GAME STATE CHANGED - End");
            Application.Quit();
        }

        public void Exit()
        {
            //
        }

        public void FixedTick()
        {
            //
        }

        public void Tick()
        {
            //
        }
    }
}

