using RTS.States.Base;
using UnityEngine;

namespace RTS.States
{
    public class ActiveState : IState
    {
        public void Enter()
        {
            Debug.Log("GAME STATE CHANGED - Active");
            Time.timeScale = 1;
        }

        public void Exit()
        {
            //
        }

        public void FixedTick()
        {
            //
        }

        public void Tick()
        {
            //
        }
    }
}