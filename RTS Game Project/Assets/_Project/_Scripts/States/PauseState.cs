using RTS.States.Base;
using UnityEngine;

namespace RTS.States
{
    public class PauseState : IState
    {
        public void Enter()
        {
            Debug.Log("GAME STATE CHANGED - Pause");
            Time.timeScale = 0;
        }

        public void Exit()
        {
            
        }

        public void FixedTick()
        {
            //
        }

        public void Tick()
        {
            //
        }
    }
}
