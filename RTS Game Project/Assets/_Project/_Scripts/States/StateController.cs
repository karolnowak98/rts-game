using RTS.States.Base;
using Zenject;

namespace RTS.States
{
    public class StateController : StateMachine
    {
        private IState activeState;
        private IState endState;
        private IState mainMenuState;
        private IState pauseState;
        private IState escState;

        [Inject]
        public void Construct(ActiveState activeState, 
                              EndState endState, 
                              MainMenuState mainMenuState, 
                              PauseState pauseState, 
                              EscState escState)
        {
            this.escState = escState;
            this.activeState = activeState;
            this.endState = endState;
            this.mainMenuState = mainMenuState;
            this.pauseState = pauseState;
        }

        private void Start()
        {
            ChangeStateToMenu();
        }

        public void ChangeStateToMenu()
        {
            ChangeState(mainMenuState);
        }

        public bool IsActiveState()
        {
            if(CurrentState == activeState)
            {
                return true;
            }
            return false;
        }

        public bool IsEscState()
        {
            if (CurrentState == escState)
            {
                return true;
            }
            return false;
        }

        public bool IsMainMenuState()
        {
            if (CurrentState == mainMenuState)
            {
                return true;
            }
            return false;
        }

        public void ChangeStateToPause()
        {
            ChangeState(pauseState);
        }

        public void ChangeStateToActive()
        {
            ChangeState(activeState);
        }

        public void ChangeStateToEnd()
        {
            ChangeState(endState);
        }

        public void ChangeStateToEsc()
        {
            ChangeState(escState);
        }
    }
}