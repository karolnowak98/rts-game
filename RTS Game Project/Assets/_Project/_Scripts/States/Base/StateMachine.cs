using UnityEngine;

namespace RTS.States.Base
{
    public class StateMachine : MonoBehaviour
    {
        private IState currentState;

        public IState CurrentState
        {
            get => currentState;
        }

        public void ChangeState(IState newState)
        {
            if (currentState == newState)
                return;

            ChangeStateRoutine(newState);
        }

        public void ChangeStateRoutine(IState newState)
        {
            if (currentState != null)
                currentState.Exit();

            currentState = newState;

            if (currentState != null)
                currentState.Enter();
        }

        private void Update()
        {
            if (currentState != null)
                currentState.Tick();
        }

        public void FixedUpdate()
        {
            if (currentState != null)
                currentState.FixedTick();
        }
    }
}