using UnityEngine;
using Zenject;

namespace RTS.Units.Player
{
    public class PlayerUnitFactory : IFactory<Object, PlayerUnit>
    {
        private readonly DiContainer container;

        public PlayerUnitFactory(DiContainer container)
        {
            this.container = container;
        }

        public PlayerUnit Create(Object prefab)
        {
            return container.InstantiatePrefabForComponent<PlayerUnit>(prefab);
        }
    }
}