using UnityEngine;
using UnityEngine.AI;
using Zenject;
using RTS.Units.Base;
using UnityEngine.InputSystem;
using RTS.Input.Base;
using RTS.Other;
using RTS.Other.Base;

namespace RTS.Units.Player
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class PlayerUnit : Unit, ISelectionable, IMovable, IMouseResponsive, IKillable
    {
        [SerializeField] private Highlight highlight;

        private NavMeshAgent navMeshAgent;
        private PlayerUnitsController unitsController;
        private Vector3 worldDeltaPosition;
        private Vector2 groundDeltaPosition;
        private bool isSelected;

        public Vector2 velocity = Vector2.zero;
        public bool shouldMove;

        [Inject]
        protected override void Construct(PlayerUnitsController unitsController,
                                          PlayerInput playerInput)
        {
            base.Construct(unitsController, playerInput);
            this.unitsController = unitsController;
        }

        protected override void Start()
        {
            base.Start();
            navMeshAgent = GetComponent<NavMeshAgent>();
            navMeshAgent.speed = attributes.baseStats.moveSpeed;
            navMeshAgent.updatePosition = false;
            transform.SetParent(unitsController.GetParent(attributes.unitType));
        }

        protected override void Update() 
        {
            base.Update();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            HealthBelowZero += Kill;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            HealthBelowZero -= Kill;
        }

        private void OnAnimatorMove()
        {
            transform.position = navMeshAgent.nextPosition;
        }

        public void Kill()
        {
            Destroy(gameObject);
        }

        public void StopUnit()
        {
            navMeshAgent.SetDestination(gameObject.transform.position);
        }

        public void SetPosition(Vector3 spawnPosition)
        {
            Vector3 calculatedSpawnPosition = spawnPosition;
            calculatedSpawnPosition.y = Utils.GetHeightOfTerrain(spawnPosition);
            navMeshAgent.Warp(calculatedSpawnPosition);
        }

        public void Move(Vector3 destination)
        {
            Vector3 calculatedDestination = destination;
            calculatedDestination.y = Utils.GetHeightOfTerrain(destination);
            navMeshAgent.SetDestination(calculatedDestination);
        }

        public void Select()
        {
            isSelected = true;
            highlight.SetAlpha(1f);
            highlight.Show();
        }

        public void Deselect()
        {
            isSelected = false;
            highlight.Hide();
            highlight.SetAlpha(0.5f);
        }
        
        public void MouseOver()
        {
            highlight.Show();
            healthBar.Show();
        }

        public void MouseExit()
        {
            if (!isSelected)
            {
                highlight.Hide();
            }
            healthBar.Hide();
        }

        /*
        private void HandleMovingState()
        {
            Debug.Log(animator);
            worldDeltaPosition = navMeshAgent.nextPosition - transform.position;
            groundDeltaPosition.x = Vector3.Dot(transform.right, worldDeltaPosition);
            groundDeltaPosition.y = Vector3.Dot(transform.forward, worldDeltaPosition);
            velocity = (Time.deltaTime > 1e-5f) ? groundDeltaPosition / Time.deltaTime : velocity = Vector2.zero;
            shouldMove = velocity.magnitude > 0.025f && navMeshAgent.remainingDistance > navMeshAgent.radius;
            animator.SetBool("move", shouldMove);
            animator.SetFloat("velx", velocity.x);
            animator.SetFloat("vely", velocity.y);
        }
        */

        public class Factory : PlaceholderFactory<Object, PlayerUnit>
        {
        }
    }
}