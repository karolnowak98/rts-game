using RTS.Resources;
using RTS.Resources.Signals;
using RTS.Selection;
using System.Collections;
using UnityEngine;
using Zenject;


namespace RTS.Units.Player
{
    [RequireComponent(typeof(Animator))]
    public class GatheringUnit : MonoBehaviour
    {
        [SerializeField] private Animator animator;
        [SerializeField] private PlayerUnit playerUnit;
        [SerializeField] private Transform pickaxe;
        [SerializeField] private GameObject mainBuildings;
        
        private SelectionController selectionController;
        private ResourcesController resourcesController;
        private SignalBus signalBus;
        private GameObject resource;
        private Transform closestMainHall;
        private float minResourceDistance;
        private float resourceGatheringTime;

        private enum State
        {
            Idle,
            MovingToResourceNode,
            GatheringResources,
            MovingToStorage,
        }

        private State state;

        [Inject]
        public void Construct(SelectionController selectionController,
                              ResourcesController resourcesController,
                              SignalBus signalBus)
        {

            this.selectionController = selectionController;
            this.resourcesController = resourcesController;
            this.signalBus = signalBus;
        }

        private void Start()
        {
            state = State.Idle;
        }

        private void OnEnable()
        {
            signalBus.Subscribe<ClickOnResourceSignal>(x => ChangeStateToMoving(x.resource));
        }

        private void OnDisable()
        {
            signalBus.TryUnsubscribe<ClickOnResourceSignal>(x => ChangeStateToMoving(x.resource));
        }

        private void ChangeStateToMoving(GameObject resource)
        {
            // if (selectionController.SelectedWorkers.Contains(this.gameObject.transform))
            // {
            //     this.resource = resource;
            //     //playerUnit.MoveUnit(resource.transform.position);
            //     state = State.MovingToResourceNode;

            //     if (resource.CompareTag("Tree"))
            //     {
            //         minResourceDistance = resourcesController.treeMinDistance;
            //         resourceGatheringTime = resourcesController.woodGatheringTime;
            //     }

            //     if (resource.CompareTag("Stone"))
            //     {
            //         minResourceDistance = resourcesController.stoneMinDistance;
            //         resourceGatheringTime = resourcesController.stoneGatheringTime;
            //     }
            // }
        }

        private void Update()
        {
            if(state == State.MovingToResourceNode)
            {
                float dist = Vector3.Distance(this.transform.position, resource.transform.position);
                if (dist < minResourceDistance)
                {
                    StartCoroutine(StartGathering(resourceGatheringTime));
                }
            }

            if(state == State.MovingToStorage)
            {
                float dist = Vector3.Distance(this.transform.position, closestMainHall.position);
                if (dist < resourcesController.mainBuildingMinDistance)
                {
                    if (resource.CompareTag("Tree"))
                    {
                        signalBus.Fire<BringWoodSignal>();
                    }

                    if (resource.CompareTag("Stone"))
                    {
                        signalBus.Fire<BringStoneSignal>();
                    }
                    playerUnit.Move(resource.transform.position);
                    state = State.MovingToResourceNode;
                }
            }
        }

        private IEnumerator StartGathering(float seconds)
        {
            if(state == State.MovingToResourceNode)
            {
                playerUnit.StopUnit();
                state = State.GatheringResources;
                pickaxe.gameObject.SetActive(true);
                animator.Play("Mining");
                //playerUnit.wasInterrupted = false;
            }

            yield return new WaitForSeconds(seconds);

            if(state == State.GatheringResources)
            {
                //if (!playerUnit.wasInterrupted)
                {
                    closestMainHall = resourcesController.GetClosestMainBuilding(this.transform);
                    state = State.MovingToStorage;
                    if(closestMainHall != null)
                    {
                        pickaxe.gameObject.SetActive(false);
                        animator.SetBool("mine", false);
                        playerUnit.Move(closestMainHall.position);
                    }
                }
            }
        }
    }
}