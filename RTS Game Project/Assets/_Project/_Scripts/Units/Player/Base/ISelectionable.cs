public interface ISelectionable
{
    void Select();
    void Deselect();
}