using RTS.Selection;
using UnityEngine;
using Zenject;
using RTS.Input;
using UnityEngine.InputSystem;
using RTS.Units.Base;

namespace RTS.Units.Player
{
    public class PlayerUnitsController : MonoBehaviour
    {
        [SerializeField] private Transform Workers;
        [SerializeField] private Transform Warriors;
        [SerializeField] private Transform Archers;
        
        private SelectionController selectionController;
        private InputController inputController;
        private SignalBus signalBus;
        private PlayerInput playerInput;

        [Inject]
        public void Construct(SelectionController selectionController,
                                InputController inputController,
                                SignalBus signalBus,
                                PlayerInput playerInput)
        {
            this.selectionController = selectionController;
            this.inputController = inputController;
            this.signalBus = signalBus;
            this.playerInput = playerInput;
        }

        private void OnEnable()
        {
            playerInput.actions["RMB"].performed += SetActionForUnits;
        }

        private void OnDisable()
        {
            if(playerInput != null)
            {
                playerInput.actions["RMB"].performed -= SetActionForUnits;
            }
        }

        public Transform GetParent(UnitSO.UnitType unitType)
        {
            switch (unitType)
            {
                case UnitSO.UnitType.Workers:
                    return Workers;
                case UnitSO.UnitType.Warriors:
                    return Warriors;
                case UnitSO.UnitType.Archers:
                    return Archers;
                default:
                    return null;
            }
        }

        public void SetActionForUnits(InputAction.CallbackContext ctx)
        {
            if(!selectionController.HasSelectedUnit) return;

            switch(inputController.hitLayer)
            {
                default:
                    MoveSelectedUnits(inputController.hit.point);
                    break;
            }
        }

        private void MoveSelectedUnits(Vector3 destination)
        {
            foreach (Transform unit in selectionController.SelectedUnits)
            {
                IMovable movable = unit.GetComponent<IMovable>();
                if(movable != null)
                {
                    movable.Move(destination);
                }
            }
        }
    }
}