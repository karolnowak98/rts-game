using UnityEngine;
using UnityEngine.AI;
using Zenject;
using RTS.Units.Player;
using UnityEngine.UI;
using RTS.Input;
using RTS.Units.Base;

namespace RTS.Units.Enemy
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class EnemyUnit : MonoBehaviour
    {
        [SerializeField] private UnitSO unitAttributes;

        private InputController inputController;
        private NavMeshAgent navMeshAgent;
        private RectTransform healthBarCanvas;
        private RectTransform healthBarForeGround;
        private MeshRenderer meshRenderer;
        private UnitStatTypes.BaseStats baseStats;
        private Collider[] rangeColliders;
        private Transform aggroTarget;
        private PlayerUnit aggroUnit;
        private Ray ray;
        private RaycastHit hit;
        private float attackCooldown;
        private float distance;
        private bool hasAggro = false;
        private int currentHealth;
        private float aggroRange;

        public bool isVisible = false;
        public int CurrentHealth
        {
            get => currentHealth;
            set
            {
                float previousHealth = currentHealth;
                if (previousHealth != value)
                {
                    currentHealth = value;
                    HealthChanged?.Invoke();
                }
            }
        }
        public delegate void HealthChangedHandler();
        public event HealthChangedHandler HealthChanged;

        [Inject]
        public void Construct(InputController inputController,
                              NavMeshAgent navMeshAgent, 
                              RectTransform healthBarCanvas,
                              [Inject(Id = "ForeGround")] RectTransform healthBarForeGround,
                              MeshRenderer meshRenderer)
        {
            this.inputController = inputController;
            this.navMeshAgent = navMeshAgent;
            this.healthBarCanvas = healthBarCanvas;
            this.healthBarForeGround = healthBarForeGround;
            this.meshRenderer = meshRenderer;
        }

        private void OnEnable()
        {
            CurrentHealth = baseStats.health;
            HealthChanged += UpdateHealthBarLevel;
        }

        private void OnDisable()
        {
            HealthChanged -= UpdateHealthBarLevel;
        }

        private void Awake()
        {
            SetProperties();
        }

        private void Update()
        {
            HandleMouseOver();
            HandleHealthBarPosition();
            attackCooldown -= Time.deltaTime;

            if (!hasAggro)
            {
                CheckForEnemyTargets();
            }
            else
            {
                MoveToAggroTarget();
                Attack();
            }
            HandleVisibleState();
        }

        private void HandleVisibleState()
        {
            if (meshRenderer.isVisible)
            {
                isVisible = true;
            }
            else
            {
                isVisible = false;
            }
        }

        private void SetProperties()
        {
            attackCooldown = unitAttributes.baseStats.attackSpeed;
            baseStats = unitAttributes.baseStats;
            CurrentHealth = baseStats.health;
        }

        private void CheckForEnemyTargets()
        {
            rangeColliders = Physics.OverlapSphere(transform.position, aggroRange);

            for (int i = 0; i < rangeColliders.Length; i++)
            {
                if (rangeColliders[i].gameObject.layer == 8)
                {
                    aggroTarget = rangeColliders[i].gameObject.transform;
                    aggroUnit = aggroTarget.gameObject.GetComponent<PlayerUnit>();
                    hasAggro = true;
                    break;
                }
            }
        }

        private void Attack()
        {
            aggroRange = 999;
            if (attackCooldown <= 0 && distance <= baseStats.attackRange + 1)
            {                
                //aggroUnit.TakeDamage(baseStats.attackDamage);
                //if(aggroUnit.hasAggro == false && aggroUnit.wasInterrupted == false)
                {
                    //aggroUnit.CheckTarget(this);
                }
                attackCooldown = baseStats.attackSpeed;
            }
        }

        private void MoveToAggroTarget()
        {
            if (aggroTarget == null)
            {
                navMeshAgent.SetDestination(transform.position);
                hasAggro = false;
            }
            else
            {
                distance = Vector3.Distance(aggroTarget.position, transform.position);
                navMeshAgent.stoppingDistance = (baseStats.attackRange + 1);

                if (distance <= aggroRange)
                {
                    navMeshAgent.SetDestination(aggroTarget.position);
                }
            }
        }

        private void HandleHealthBarPosition()
        {
            healthBarCanvas.transform.LookAt(healthBarCanvas.transform.position +
                Camera.main.transform.rotation * Vector3.forward,
                Camera.main.transform.rotation * Vector3.up);
        }

        public void TakeDamage(int damage)
        {
            int totalDamage = damage - baseStats.armor;
            if (totalDamage <= 0) totalDamage = 1;
            CurrentHealth -= totalDamage;
            if (CurrentHealth <= 0)
            {
                Die();
            }
        }

        private void HandleMouseOver()
        {
            ray = Camera.main.ScreenPointToRay(Utils.MousePos());
            if (Physics.Raycast(ray, out hit))
            {
                if (isVisible)
                {
                    if (hit.transform == this.gameObject.transform)
                    {
                        healthBarCanvas.gameObject.SetActive(true);
                    }
                    else
                    {
                        if (!inputController.leftAltClicked)
                        {
                            healthBarCanvas.gameObject.SetActive(false);
                        }
                    }
                }
            }
        }

        private void UpdateHealthBarLevel()
        {
            Image healthBar = healthBarForeGround.GetComponent<Image>();
            float currentHealthPct = (float)CurrentHealth / (float)baseStats.health;
            healthBar.fillAmount = currentHealthPct;
        }
        
        private void Die()
        {               
            Destroy(gameObject);
        }
    }
}

