using UnityEngine;

namespace RTS.Units.Base
{
    [CreateAssetMenu(fileName = "New Unit", menuName = "New Unit")]
    public class UnitSO : ScriptableObject
    {
        public enum UnitType
        {
            Workers,
            Warriors,
            Archers,
            Enemies
        }

        [Space(15)]
        [Header("Unit Settings")]

        public new string name;
        public Sprite icon;
        public GameObject prefab;
        public UnitType unitType;
        public bool isPlayerUnit;

        [Space(15)]
        [Header("Unit Cost")]
        [Space(20)]

        public int woodCost;
        public int stoneCost;

        [Space(15)]
        [Header("Unit Base Stats")]
        [Space(40)]

        public UnitStatTypes.BaseStats baseStats;
    }
}