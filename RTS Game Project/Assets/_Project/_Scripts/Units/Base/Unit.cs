using RTS.Other;
using RTS.Other.Base;
using RTS.Units.Player;
using UnityEngine;
using UnityEngine.InputSystem;

namespace RTS.Units.Base
{
    [RequireComponent(typeof(Animator))]
    public class Unit : MonoBehaviour, IDamageable
    {
        [SerializeField] protected HealthBar healthBar;

        protected Animator animator;
        protected delegate void HealthChangedHandler();
        protected delegate void HealthBelowZeroHandler();
        protected event HealthChangedHandler HealthChanged;
        protected event HealthBelowZeroHandler HealthBelowZero;

        protected int health;
        protected int Health
        {
            get => Health;
            set
            {
                int previousHealth = health;

                if (previousHealth != value)
                {
                    health = value;
                    HealthChanged?.Invoke();
                }

                if (health <= 0)
                {
                    HealthBelowZero?.Invoke();
                }
            }
        }
        protected int armor;
        
        private Camera cam;
        private PlayerInput playerInput;

        public UnitSO attributes;

        protected virtual void Construct(PlayerUnitsController playerUnitsController,
                                         PlayerInput playerInput)
        {
            this.playerInput = playerInput;
        }

        protected virtual void Start()
        {
            animator = GetComponent<Animator>();
            armor = attributes.baseStats.armor;
            Health = attributes.baseStats.health;
            cam = Camera.main;
        }

        protected virtual void OnEnable()
        {
            //playerInput.actions["LeftAlt"].performed += _ => healthBar.Show();
            //playerInput.actions["LeftAlt"].canceled += _ => healthBar.Hide();
        }

        protected virtual void OnDisable()
        {
            if(playerInput == null) return;
            //playerInput.actions["LeftAlt"].performed -= _ => healthBar.Show();
            //playerInput.actions["LeftAlt"].canceled += _ => healthBar.Hide();
        }

        protected virtual void Update()
        {
            healthBar.FaceHealthBarToCamera(Camera.main);
        }

        public void TakeDamage(int damage)
        {
            int updatedDamage = damage - attributes.baseStats.armor;

            Health -= updatedDamage;
        }
    }
}