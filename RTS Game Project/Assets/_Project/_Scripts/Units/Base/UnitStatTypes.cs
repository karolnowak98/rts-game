using System;
using UnityEngine;

namespace RTS.Units.Base
{
    public class UnitStatTypes : ScriptableObject
    {
        [Serializable]
        public class BaseStats
        {
            public int health, armor, attackDamage;
            public float attackRange, attackSpeed, moveSpeed, aggroRange, fov, nightFov;
        }
    }
}