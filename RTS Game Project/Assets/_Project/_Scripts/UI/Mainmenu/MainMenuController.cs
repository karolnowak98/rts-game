using RTS.States;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using Zenject;

namespace RTS.UI.Mainmenu
{
    public class MainMenuController : MonoBehaviour
    {
        [SerializeField] private GameObject mainMenu;
        [SerializeField] private GameObject optionsMenu;
        [SerializeField] private GameObject credits;

        private StateController stateController;
        private bool enterClicked;
        private bool escapeClicked;
        private Vector2 moveAxis;
        private int mainMenuIndex = 0;
        private int mainMenuMaxIndex = 3;
        private int subMenuIndex = 0;
        
        [Inject]
        public void Construct(StateController stateController)
        {
            this.stateController = stateController;
        }

        private void SetRightActionForEnter()
        {
            switch (subMenuIndex)
            {
                case 0:
                    switch (mainMenuIndex)
                    {
                        case 0:
                            StartGame();
                            break;
                        case 1:
                            Options();
                            break;
                        case 2:
                            Credits();
                            break;
                        case 3:
                            ExitGame();
                            break;
                    }
                    break;
                default:
                    Back();
                    break;
            }
        }

        private void SetRightActionForEscape()
        {
            switch (subMenuIndex)
            {
                case 0:
                    break;
                default:
                    Back();
                    break;
            }
        }

        public void StartGame()
        {
            SceneManager.LoadScene("Game");
            stateController.ChangeStateToActive();
        }

        public void Back()
        {
            mainMenu.SetActive(true);
            optionsMenu.SetActive(false);
            subMenuIndex = 0;
            mainMenuIndex = 0;
        }

        public void Options()
        {
            mainMenu.SetActive(false);
            optionsMenu.SetActive(true);
            subMenuIndex = 1;
            mainMenuIndex = 0;
        }

        public void Credits()
        {
            mainMenu.SetActive(false);
            credits.SetActive(true);
            subMenuIndex = 2;
            mainMenuIndex = 0;
        }

        public void ExitGame()
        {
            stateController.ChangeStateToEnd();
        }

        private void DecreaseMenuIndex()
        {
            if (mainMenuIndex < mainMenuMaxIndex)
            {
                mainMenuIndex++;
            }
        }

        private void IncreaseMenuIndex()
        {
            if (mainMenuIndex > 0)
            {
                mainMenuIndex--;
            }
        }

        public void Enter(InputAction.CallbackContext ctx)
        {
            enterClicked = ctx.ReadValueAsButton();

            if (enterClicked && ctx.performed)
            {
                SetRightActionForEnter();
            }
        }

        public void Move(InputAction.CallbackContext ctx)
        {
            moveAxis = ctx.ReadValue<Vector2>();
            if (moveAxis.y == -1 && ctx.performed)
            {
                DecreaseMenuIndex();
            }

            if (moveAxis.y == 1 && ctx.performed)
            {
                IncreaseMenuIndex();
            }
        }

        public void Back(InputAction.CallbackContext ctx)
        {
            escapeClicked = ctx.ReadValueAsButton();
            if (escapeClicked && ctx.performed)
            {
                SetRightActionForEscape();
            }
        }
    }
}