using UnityEngine;
using Zenject;
using RTS.Units.Player;
using RTS.Selection;
using RTS.Resources;
using RTS.Units.Base;
using RTS.Input;
using RTS.Input.Base;
using RTS.Game.ActionButtons.Base;

namespace RTS.Game.ActionButtons
{
    public class FirstButton : NumberButton, IMouseResponsive
    {
        [SerializeField] private UnitSO worker;
        [SerializeField] private UnitSO warrior;
        [SerializeField] private Sprite buildIcon;
        [SerializeField] private GameObject actionInfo;

        private SelectionController selectionController;
        private InputController inputController;
        private PlayerUnit.Factory unitFactory;
        private PlayerResources resource;

        [Inject]
        public void Construct(SelectionController selectionController,
                              InputController inputController,
                              PlayerUnit.Factory unitFactory, 
                              PlayerResources resource)
        {
            this.selectionController = selectionController;
            this.inputController = inputController;
            this.unitFactory = unitFactory;
            this.resource = resource;
        }

        protected override void Awake()
        {
            base.Awake();
        }

        private void Update()
        {
            
        }

        protected override void ButtonClicked()
        {
            if (selectionController.HasSelectedMain)
            {
                foreach (Transform building in selectionController.SelectedMainBuildings)
                {
                    //RecruitUnit(worker, building.gameObject);
                }
            }
            
            else if (selectionController.HasSelectedMilitary)
            {
                foreach (Transform building in selectionController.SelectedMilitaryBuildings)
                {
                    //RecruitUnit(warior, building.gameObject);
                }
            }
        }
       
        protected override void SetRightIcon()
        {
            if (selectionController.SelectedMainBuildingsNumber > 0)
            {
                image.sprite = worker.icon;
            }

            else if (selectionController.SelectedMilitaryBuildingsNumber > 0)
            {
                image.sprite = warrior.icon;
            }

            else if (selectionController.SelectedWorkersNumber > 0)
            {
                image.sprite = buildIcon;
            }

            else
            {
                image.sprite = blankImage;
            }
        }

        public void MouseOver() 
        {
            actionInfo.SetActive(true);
        }

        public void MouseExit() 
        {
            actionInfo.SetActive(false);
        }
    }
}