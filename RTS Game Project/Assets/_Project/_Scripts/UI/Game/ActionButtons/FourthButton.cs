using UnityEngine;
using Zenject;
using RTS.Units.Player;
using RTS.Selection;
using RTS.Resources;
using RTS.Units.Base;
using RTS.Game.ActionButtons.Base;

namespace RTS.Game.ActionButtons
{
    public class FourthButton : NumberButton
    {
        [SerializeField] private UnitSO worker;
        [SerializeField] private UnitSO warrior;
        [SerializeField] private Sprite buildIcon;

        private SelectionController selectionController;
        private PlayerUnit.Factory unitFactory;
        private PlayerResources resource;

        [Inject]
        public void Construct(SelectionController selectionController,
                              PlayerUnit.Factory unitFactory,
                              PlayerResources resource)
        {
            this.selectionController = selectionController;
            this.unitFactory = unitFactory;
            this.resource = resource;
        }

        protected override void Awake()
        {
            base.Awake();
        }

        protected override void ButtonClicked()
        {
            //
        }

        protected override void SetRightIcon()
        {
            //
        }
    }
}