using RTS.Selection.Signals;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace RTS.Game.ActionButtons.Base
{
    [RequireComponent(typeof(Button))]
    [RequireComponent(typeof(Image))]
    public abstract class NumberButton : MonoBehaviour
    {
        [SerializeField] protected Sprite blankImage;

        protected Button button;
        protected Image image;

        private SignalBus signalBus;

        [Inject]
        public void Construct(SignalBus signalBus)
        {
            this.signalBus = signalBus;
        }

        protected virtual void Awake()
        {
            button = GetComponent<Button>();
            image = GetComponent<Image>();
        }

        private void OnEnable()
        {
            button.onClick.AddListener(() => ButtonClicked());
            signalBus.Subscribe<SelectionChangedSignal>(SetRightIcon);
        }

        private void OnDisable()
        {
            button.onClick.RemoveListener(() => ButtonClicked());
            signalBus.Unsubscribe<SelectionChangedSignal>(SetRightIcon);
        }

        protected abstract void ButtonClicked();
        protected abstract void SetRightIcon();
    }
}