using UnityEngine;
using Zenject;
using RTS.Units.Player;
using RTS.Selection;
using RTS.Resources;
using RTS.Units.Base;
using RTS.Game.ActionButtons.Base;

namespace RTS.Game.ActionButtons
{
    public class ThirdButton : NumberButton
    {
        [SerializeField] private UnitSO worker;
        [SerializeField] private UnitSO warrior;
        [SerializeField] private Sprite buildIcon;

        private SelectionController selectionController;
        private PlayerUnit.Factory unitFactory;
        private PlayerResources resource;

        [Inject]
        public void Construct(SelectionController selectionController,
                              PlayerUnit.Factory unitFactory,
                              PlayerResources resource)
        {
            this.selectionController = selectionController;
            this.unitFactory = unitFactory;
            this.resource = resource;
        }

        protected override void Awake()
        {
            base.Awake();
        }

        protected override void ButtonClicked()
        {
            if (selectionController.HasSelectedMain)
            {
                foreach (Transform building in selectionController.SelectedMainBuildings)
                {
                    RecruitUnit(worker, building.gameObject);
                }
            }

            else if (selectionController.HasSelectedMilitary)
            {
                foreach (Transform building in selectionController.SelectedMilitaryBuildings)
                {
                    RecruitUnit(warrior, building.gameObject);
                }
            }
        }

        protected override void SetRightIcon()
        {
            if (selectionController.HasSelectedMain)
            {
                image.sprite = worker.icon;
            }

            else if (selectionController.HasSelectedMilitary)
            {
                image.sprite = warrior.icon;
            }

            else if (selectionController.HasSelectedWorker)
            {
                image.sprite = buildIcon;
            }

            else
            {
                image.sprite = blankImage;
            }
        }

        private void RecruitUnit(UnitSO unitType, GameObject building)
        {
            if (resource.StoneNumber >= unitType.stoneCost && resource.WoodNumber >= unitType.woodCost)
            {
                Vector3 spawnPoint = building.transform.Find("SpawnPoint").position;
                Vector3 destinationPoint = building.transform.Find("DestinationPoint").position;

                resource.StoneNumber -= unitType.stoneCost;
                resource.WoodNumber -= unitType.woodCost;

                GameObject unit = unitFactory.Create(unitType.prefab).gameObject;
                unit.GetComponent<PlayerUnit>().SetPosition(spawnPoint);
                //unit.GetComponent<PlayerUnit>().MoveUnit(destinationPoint);
            }
        }
    }
}