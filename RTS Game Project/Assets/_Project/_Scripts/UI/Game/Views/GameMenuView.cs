using RTS.States;
using RTS.UI.Views.Base;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace RTS.UI.Views
{
    public class GameMenuView : View
    {
        private StateController stateController;
        private Vector2 moveAxis;
        private int mainMenuIndex = 0;
        private int mainMenuMaxIndex = 2;
        private bool enterClicked;
        private bool escapeClicked;

        [Inject]
        public void Construct(StateController stateController)
        {
            this.stateController = stateController;
        }

        private void SetRightActionForEnter()
        {
            switch (mainMenuIndex)
            {
                case 0:
                    ResumeGame();
                    break;
                case 1:
                    GoToMainMenu();
                    break;
                case 2:
                    ExitGame();
                    break;
            }
        }

        private void DecreaseMenuIndex()
        {
            if (mainMenuIndex < mainMenuMaxIndex)
            {
                mainMenuIndex++;
            }
            else
            {
                mainMenuIndex = 0;
            }
        }

        private void IncreaseMenuIndex()
        {
            if (mainMenuIndex > 0)
            {
                mainMenuIndex--;
            }
        }

        public void Move(InputAction.CallbackContext ctx)
        {
            moveAxis = ctx.ReadValue<Vector2>();
            if (moveAxis.y == -1 && ctx.performed)
            {
                DecreaseMenuIndex();
            }

            if (moveAxis.y == 1 && ctx.performed)
            {
                IncreaseMenuIndex();
            }
        }

        public void Back(InputAction.CallbackContext ctx)
        {
            escapeClicked = ctx.ReadValueAsButton();
            if (escapeClicked && ctx.performed)
            {
                ResumeGame();
            }
        }

        public void Enter(InputAction.CallbackContext ctx)
        {
            enterClicked = ctx.ReadValueAsButton();

            if (enterClicked && ctx.performed)
            {
                SetRightActionForEnter();
            }
        }

        public void ResumeGame()
        {
            Hide();
            stateController.ChangeStateToActive();
        }

        public void GoToMainMenu()
        {
            stateController.ChangeStateToMenu();
        }

        public void ExitGame()
        {
            stateController.ChangeStateToEnd();
        }
    }
}