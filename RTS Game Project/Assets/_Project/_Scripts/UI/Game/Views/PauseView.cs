using RTS.States;
using RTS.UI.Views.Base;
using Zenject;

namespace RTS.UI.Views
{
    public class PauseView : View
    {
        private StateController stateController;

        [Inject]
        public void Construct(StateController stateController)
        {
            this.stateController = stateController;
        }

        public void ResumeGame()
        {
            Hide();
            stateController.ChangeStateToActive();
        }
    }
}
