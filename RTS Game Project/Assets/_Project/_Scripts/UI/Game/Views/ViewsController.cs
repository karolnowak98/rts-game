using RTS.States;
using RTS.UI.Views.Signals;
using UnityEngine;
using Zenject;

namespace RTS.UI.Views
{
    public class ViewsController : MonoBehaviour
    {
        [SerializeField] private PauseView pauseView;
        [SerializeField] private GameMenuView menuView;

        private SignalBus signalBus;
        private StateController stateController;

        [Inject]
        public void Construct(SignalBus signalBus,
                            StateController stateController)
        {
            this.signalBus = signalBus;
            this.stateController = stateController;
        }

        private void OnEnable()
        {
            signalBus.Subscribe<ShowGameMenuSignal>(menuView.Show);
            signalBus.Subscribe<PauseUnpauseGameSignal>(PauseUnpauseGame);
        }

        private void OnDisable()
        {
            signalBus.Unsubscribe<ShowGameMenuSignal>(menuView.Show);
            signalBus.Unsubscribe<PauseUnpauseGameSignal>(PauseUnpauseGame);
        }

        private void PauseUnpauseGame()
        {
            if (stateController.IsActiveState())
            {
                stateController.ChangeStateToPause();
                pauseView.Show();
            }

            else
            {
                stateController.ChangeStateToActive();
                pauseView.Hide();
            }
        }
    }
}