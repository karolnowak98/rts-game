using RTS.Resources.Signals;
using RTS.Units.Base;
using TMPro;
using UnityEngine;
using Zenject;

namespace RTS.Resources
{
    public class ResourcesController : MonoBehaviour
    {
        [SerializeField] private int woodGatherNumber;
        [SerializeField] private int stoneGatherNumber;
        [SerializeField] private TMP_Text woodNumber;
        [SerializeField] private TMP_Text stoneNumber;
        [SerializeField] private Transform mainBuildings;

        private SignalBus signalBus;
        private PlayerResources resources;

        public float mainBuildingMinDistance;
        public float treeMinDistance;
        public float stoneMinDistance;
        public float woodGatheringTime;
        public float stoneGatheringTime;

        [Inject]
        public void Construct(SignalBus signalBus, 
                              PlayerResources resource)
        {
            this.signalBus = signalBus;
            this.resources = resource;
        }

        private void Start()
        {
            UpdateWoodNumber();
            UpdateStoneNumber();
        }

        private void OnEnable()
        {
            signalBus.Subscribe<BringStoneSignal>(AddStone);
            signalBus.Subscribe<BringWoodSignal>(AddWood);
            resources.WoodNumberChanged += UpdateWoodNumber;
            resources.StoneNumberChanged += UpdateStoneNumber;
        }

        private void OnDisable()
        {
            signalBus.Unsubscribe<BringStoneSignal>(AddStone);
            signalBus.Unsubscribe<BringWoodSignal>(AddWood);
            resources.WoodNumberChanged -= UpdateWoodNumber;
            resources.StoneNumberChanged -= UpdateStoneNumber;
        }

        private void UpdateWoodNumber()
        {
            woodNumber.text = resources.WoodNumber.ToString();
        }

        private void UpdateStoneNumber()
        {
            stoneNumber.text = resources.StoneNumber.ToString();
        }

        private void AddWood()
        {
            resources.WoodNumber += woodGatherNumber;
        }

        private void AddStone()
        {
            resources.StoneNumber += stoneGatherNumber;
        }

        public void ConsumeResources(int woodNumber, int stoneNumber)
        {
            resources.WoodNumber -= woodNumber;
            resources.StoneNumber -= stoneNumber;
        }

        public bool AreEnoughResources(UnitSO unit)
        {
            if (resources.StoneNumber >= unit.stoneCost && resources.WoodNumber >= unit.woodCost)
            {
                return true;
            }
            return false;
        }

        public Transform GetClosestMainBuilding(Transform worker)
        {
            float distance = 9999;
            float maxDistance = distance;
            Transform closestMainBuilding = null;
            foreach (Transform mainBuilding in mainBuildings)
            {
                distance = Vector3.Distance(worker.position, mainBuilding.transform.position);
                if(distance < maxDistance)
                {
                    maxDistance = distance;
                    closestMainBuilding = mainBuilding;
                }
            }
            return closestMainBuilding;
        }
    }
}