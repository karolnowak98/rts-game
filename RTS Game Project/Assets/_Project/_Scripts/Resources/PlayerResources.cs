using UnityEngine;

namespace RTS.Resources
{
    public class PlayerResources
    {
        [SerializeField] private int woodNumber = 0;
        [SerializeField] private int stoneNumber = 0;

        public delegate void WoodNumberChangedHandler();
        public event WoodNumberChangedHandler WoodNumberChanged;
        public delegate void StoneNumberChangedHandler();
        public event StoneNumberChangedHandler StoneNumberChanged;

        public int WoodNumber
        {
            get => woodNumber;
            set
            {
                int previousWoodNumber = woodNumber;
                if(previousWoodNumber != value)
                {
                    woodNumber = value;
                    WoodNumberChanged?.Invoke();
                }
            }
        }

        public int StoneNumber
        {
            get => stoneNumber;
            set
            {
                int previousWoodNumber = stoneNumber;
                if (previousWoodNumber != value)
                {
                    stoneNumber = value;
                    StoneNumberChanged?.Invoke();
                }
            }
        }
    }
}