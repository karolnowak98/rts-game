using UnityEngine;

namespace RTS.Resources.Signals
{
    public class ClickOnResourceSignal
    {
        public GameObject resource;
    }
}