using RTS.Minimaps.Signals;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace RTS.Cameras
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private float cameraSpeed = 20f;
        [SerializeField] private float panBorderThickness = 10f;
        [SerializeField] private Vector2 xPanLimit;
        [SerializeField] private Vector2 zPanLimit;

        [SerializeField] private float scrollSpeed = 20f;
        [SerializeField] private float minScroll = 5f;
        [SerializeField] private float maxScroll = 20f;

        [SerializeField] private Material minimapIndicatorMaterial;
        [SerializeField] private float minimapFOVStrokeWidth = 0.05f;

        private Vector3 cameraPos;
        private Vector3 mousePos;
        private Vector2 moveAxis;
        private float scrollMove;

        private Transform minimapIndicator;
        private Mesh minimapIndicatorMesh;
        private SignalBus signalBus;

        [Inject]
        private void Construct(SignalBus signalBus)
        {
            this.signalBus = signalBus;
        }

        private void OnEnable()
        {
            signalBus.Subscribe<DraggingMouseOnMinimapSignal>(x => MoveCameraMinimap(x.mousePos));
        }

        private void OnDisable()
        {
            signalBus.TryUnsubscribe<DraggingMouseOnMinimapSignal>(x => MoveCameraMinimap(x.mousePos));
        }

        private void Awake()
        {
            
            PrepareMapIndicator();
        }

        private void Update()
        {
            HandleCameraPosition();
        }

        private void HandleCameraPosition()
        {
            cameraPos = transform.position;

            cameraPos += new Vector3(moveAxis.x * Time.deltaTime * cameraSpeed, 
                                     0,
                                     moveAxis.y * Time.deltaTime * cameraSpeed);
            HandleMouseCameraMovement();
            
            cameraPos.x = Mathf.Clamp(cameraPos.x, xPanLimit.x, xPanLimit.y);
            cameraPos.z = Mathf.Clamp(cameraPos.z, zPanLimit.x, zPanLimit.y);

            transform.position = cameraPos;
            ComputeMinimapIndicator(false);
        }

        private void HandleZoom()
        {
            Camera.main.orthographicSize -= scrollMove * scrollSpeed * Time.deltaTime;
            Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, minScroll, maxScroll);
            ComputeMinimapIndicator(true);
        }

        private void HandleMouseCameraMovement()
        {
            if (Utils.MousePos().y >= Screen.height - panBorderThickness)
            {
                cameraPos.z += cameraSpeed * Time.deltaTime;
            }

            if (Utils.MousePos().y <= panBorderThickness)
            {
                cameraPos.z -= cameraSpeed * Time.deltaTime;
            }

            if (Utils.MousePos().x >= Screen.width - panBorderThickness)
            {
                cameraPos.x += cameraSpeed * Time.deltaTime;
            }

            if (Utils.MousePos().x <= panBorderThickness)
            {
                cameraPos.x -= cameraSpeed * Time.deltaTime;
            }
        }

        public void CameraMoveWASD(InputAction.CallbackContext ctx)
        {
            moveAxis = ctx.ReadValue<Vector2>();
        }

        public void CameraZoom(InputAction.CallbackContext ctx)
        {
            scrollMove = ctx.ReadValue<float>();

            if (ctx.performed)
            {
                HandleZoom();
            }
        }

        private void PrepareMapIndicator()
        {
            GameObject g = new GameObject("MinimapIndicator");
            minimapIndicator = g.transform;
            g.layer = 25;
            minimapIndicator.position = Vector3.zero;
            minimapIndicatorMesh = CreateMinimapIndicatorMesh();
            MeshFilter mf = g.AddComponent<MeshFilter>();
            mf.mesh = minimapIndicatorMesh;
            MeshRenderer mr = g.AddComponent<MeshRenderer>();
            mr.material = new Material(minimapIndicatorMaterial);
            ComputeMinimapIndicator(true);
        }

        private Mesh CreateMinimapIndicatorMesh()
        {
            Mesh m = new Mesh();
            Vector3[] vertices = new Vector3[] {
            Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero,
            Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero
        };
            int[] triangles = new int[] {
            0, 4, 1, 4, 5, 1,
            0, 2, 6, 6, 4, 0,
            6, 2, 7, 2, 3, 7,
            5, 7, 3, 3, 1, 5
        };
            m.vertices = vertices;
            m.triangles = triangles;
            return m;
        }

        private void ComputeMinimapIndicator(bool zooming)
        {
            Vector3 middle = Utils.MiddleOfScreenPointToWorld();
            if (zooming)
            {
                Vector3[] viewCorners = Utils.ScreenCornersToWorldPoints();
                float w = viewCorners[1].x - viewCorners[0].x;
                float h = viewCorners[2].z - viewCorners[0].z;
                for (int i = 0; i < 4; i++)
                {
                    viewCorners[i].x -= middle.x;
                    viewCorners[i].z -= middle.z;
                }
                Vector3[] innerCorners = new Vector3[]
                {
                new Vector3(viewCorners[0].x + minimapFOVStrokeWidth * w, 0f, viewCorners[0].z + minimapFOVStrokeWidth * h),
                new Vector3(viewCorners[1].x - minimapFOVStrokeWidth * w, 0f, viewCorners[1].z + minimapFOVStrokeWidth * h),
                new Vector3(viewCorners[2].x + minimapFOVStrokeWidth * w, 0f, viewCorners[2].z - minimapFOVStrokeWidth * h),
                new Vector3(viewCorners[3].x - minimapFOVStrokeWidth * w, 0f, viewCorners[3].z - minimapFOVStrokeWidth * h)
                };
                Vector3[] allCorners = new Vector3[]
                {
                viewCorners[0], viewCorners[1], viewCorners[2], viewCorners[3],
                innerCorners[0], innerCorners[1], innerCorners[2], innerCorners[3]
                };
                for (int i = 0; i < 8; i++)
                    allCorners[i].y = 100f;
                minimapIndicatorMesh.vertices = allCorners;
                minimapIndicatorMesh.RecalculateNormals();
                minimapIndicatorMesh.RecalculateBounds();
            }
            minimapIndicator.position = middle;
        }

        public void MoveCameraMinimap(Vector3 pos)
        {
            Vector3 off = transform.position - Utils.MiddleOfScreenPointToWorld();
            Vector3 newPos = pos + off;

            newPos.y = transform.position.y;
            transform.position = newPos;

            ComputeMinimapIndicator(false);
        }
    }
}