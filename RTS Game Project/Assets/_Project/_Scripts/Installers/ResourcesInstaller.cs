using RTS.Resources;
using RTS.Resources.Signals;
using Zenject;

namespace RTS.Installers
{
    public class ResourcesInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.DeclareSignal<ClickOnResourceSignal>();
            Container.DeclareSignal<BringWoodSignal>();
            Container.DeclareSignal<BringStoneSignal>();
            Container.Bind<PlayerResources>().AsSingle().NonLazy();
        }
    }
}