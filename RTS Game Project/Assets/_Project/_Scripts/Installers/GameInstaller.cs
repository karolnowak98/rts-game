using Zenject;
using RTS.States;

namespace RTS.Installers
{
    public class GameInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            SignalBusInstaller.Install(Container);
            Container.Bind<StateController>().FromNewComponentOnNewGameObject().AsSingle().NonLazy();
            Container.Bind<MainMenuState>().AsSingle();
            Container.Bind<ActiveState>().AsSingle();
            Container.Bind<EndState>().AsSingle();
            Container.Bind<PauseState>().AsSingle();
            Container.Bind<EscState>().AsSingle();
        }
    }
}