using RTS.UI.Views.Signals;
using Zenject;

namespace RTS.Installers
{
    public class InputInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.DeclareSignal<ShowGameMenuSignal>();
            Container.DeclareSignal<PauseUnpauseGameSignal>();
        }
    }
}
