using System.Collections.Generic;
using RTS.Selection.Signals;
using UnityEngine;
using Zenject;

namespace RTS.Installers
{
    public class SelectionInstaller : MonoInstaller
    {
        public List<Transform> selectedUnits;

        public override void InstallBindings()
        {
            Container.DeclareSignal<SelectionChangedSignal>();
            Container.Bind<List<Transform>>().FromInstance(selectedUnits);
        }
    }
}