using Zenject;
using UnityEngine;
using RTS.Units.Player;

namespace RTS.Installers
{
    public class UnitsInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindFactory<Object, PlayerUnit, PlayerUnit.Factory>().FromFactory<PlayerUnitFactory>();
        }
    }
}