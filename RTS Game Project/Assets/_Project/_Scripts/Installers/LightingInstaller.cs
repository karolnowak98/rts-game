using RTS.Game.Lighting.Signals;
using Zenject;

namespace RTS.Installers
{
    public class LightingInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.DeclareSignal<DayComesSignal>();
            Container.DeclareSignal<NightComesSignal>();
        }
    }
}