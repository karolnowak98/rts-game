using RTS.Minimaps.Signals;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace RTS.Installers
{
    public class MinimapInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.DeclareSignal<DraggingMouseOnMinimapSignal>();
        }
    }
}