using UnityEngine;

namespace RTS.Minimaps.Signals
{
    public class DraggingMouseOnMinimapSignal
    {
        public Vector3 mousePos;
    }
}