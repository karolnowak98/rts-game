using RTS.Minimaps.Signals;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace RTS.Minimaps
{
    [RequireComponent(typeof(RectTransform))]
    public class Minimap : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private RectTransform minimapTransform;
        [SerializeField] private Vector2 terrainSize;

        private SignalBus signalBus;
        private Vector2 uiSize;
        private Vector2 uiPos;
        private Vector2 delta;
        private Vector3 realPos;
        private Vector2 lastPointerPosition;
        private bool dragging = false;
        
        [Inject]
        private void Construct(SignalBus signalBus)
        {
            this.signalBus = signalBus;
        }

        private void Start()
        {
            uiSize = minimapTransform.sizeDelta;
            lastPointerPosition = Utils.MousePos();
        }

        private void Update()
        {
            if (!dragging) return;

            delta = Utils.MousePos() - lastPointerPosition;
            lastPointerPosition = Utils.MousePos();

            if (delta.magnitude > Mathf.Epsilon)
            {
                uiPos = Utils.MousePos();
                realPos = new Vector3(
                    uiPos.x / uiSize.x * terrainSize.x,
                    0f,
                    uiPos.y / uiSize.y * terrainSize.y
                );
                signalBus.Fire(new DraggingMouseOnMinimapSignal() { mousePos = realPos });
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            dragging = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            dragging = false;
        }
    }
}