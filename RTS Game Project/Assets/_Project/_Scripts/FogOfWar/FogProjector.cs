using System.Collections;
using UnityEngine;
using Zenject;

namespace RTS.Game.FogOfWar
{
    public class FogProjector : MonoBehaviour
    {
        [SerializeField] private Material projectorMaterial;
        [SerializeField] private float blendSpeed;
        [SerializeField] private int textureScale;
        [SerializeField] private RenderTexture fogTexture;
        [SerializeField] private Projector projector;

        private RenderTexture prevTexture;
        private RenderTexture currTexture;

        private float blendAmount;

        private void Start()
        {
            //projector.enabled = true;

            prevTexture = GenerateTexture();
            currTexture = GenerateTexture();

            projector.material = new Material(projectorMaterial);

            projector.material.SetTexture("_PrevTexture", prevTexture);
            projector.material.SetTexture("_CurrTexture", currTexture);

            StartNewBlend();
        }

        private RenderTexture GenerateTexture()
        {
            RenderTexture rt = new RenderTexture(
                fogTexture.width * textureScale,
                fogTexture.height * textureScale,
                0,
                fogTexture.format)
            { 
                filterMode = FilterMode.Bilinear
            };
            rt.antiAliasing = fogTexture.antiAliasing;
            return rt;
        }

        private void StartNewBlend()
        {
            StopCoroutine(BlendFog());

            blendAmount = 0;
            Graphics.Blit(currTexture, prevTexture);
            Graphics.Blit(fogTexture, currTexture);

            StartCoroutine(BlendFog());
        }

        private IEnumerator BlendFog()
        {
            while (blendAmount < 1)
            {
                blendAmount += Time.deltaTime * blendSpeed;
                projector.material.SetFloat("_Blend", blendAmount);
                yield return null;
            }
            StartNewBlend();
        }
    }
}