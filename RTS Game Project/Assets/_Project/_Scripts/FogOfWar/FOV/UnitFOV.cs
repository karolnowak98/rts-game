using RTS.FogOfWar.FOV.Base;
using RTS.Units.Player;
using UnityEngine;

namespace RTS.FogOfWar.FOV
{
    public class UnitFOV : CircleFOV
    {
        [SerializeField] private PlayerUnit unit;

        private void Awake()
        {
            fov = unit.attributes.baseStats.fov;
            nightFov = unit.attributes.baseStats.nightFov;
        }
    }
}