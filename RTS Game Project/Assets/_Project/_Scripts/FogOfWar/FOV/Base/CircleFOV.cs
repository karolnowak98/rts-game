using RTS.Game.Lighting.Signals;
using System.Collections;
using UnityEngine;
using Zenject;

namespace RTS.FogOfWar.FOV.Base
{
    public abstract class CircleFOV : MonoBehaviour
    {
        [SerializeField] protected int quality = 200;
        [SerializeField] protected MeshFilter meshFilter;
        [SerializeField] protected MeshRenderer meshRenderer;

        protected SignalBus signalBus;
        protected float fov;
        protected float nightFov;

        [Inject]
        public void Construct(SignalBus signalBus)
        {
            this.signalBus = signalBus;
        }

        private void Start()
        {
            SetFOV(fov);
        }

        private void OnEnable()
        {
            signalBus.Subscribe<DayComesSignal>(() => SetFOV(fov));
            signalBus.Subscribe<NightComesSignal>(() => SetFOV(nightFov));
        }

        private void OnDisable()
        {
            signalBus.TryUnsubscribe<DayComesSignal>(() => SetFOV(fov));
            signalBus.TryUnsubscribe<NightComesSignal>(() => SetFOV(nightFov));
        }

        protected void SetFOV(float fov)
        {
            meshFilter.mesh = Utils.CreateCircle(fov, quality);
            meshRenderer.material = new Material(meshRenderer.material);
            StartCoroutine(ScaleFOV(fov));
        }

        protected IEnumerator ScaleFOV(float fov)
        {
            float r = 0f, t = 0f, step = 0.01f;
            float scaleUpTime = 0.05f;
            Vector3 startScale = transform.localScale;
            Vector3 endScale = fov * Vector3.one;
            endScale.z = 1f;
            do
            {
                transform.localScale = Vector3.Lerp(startScale, endScale, r);
                t += step;
                r = t / scaleUpTime;
                yield return new WaitForSecondsRealtime(step);
            } while (r < 1f);
        }
    }
}