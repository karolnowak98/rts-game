using RTS.Buildings.Player;
using RTS.FogOfWar.FOV.Base;
using UnityEngine;
using Zenject;

namespace RTS.FogOfWar.FOV
{
    public class BuildingFOV : CircleFOV
    {
        [SerializeField] private PlayerBuilding building;

        private void Awake()
        {
            fov = building.attributes.fov;
            nightFov = building.attributes.nightFov;
        }
    }
}