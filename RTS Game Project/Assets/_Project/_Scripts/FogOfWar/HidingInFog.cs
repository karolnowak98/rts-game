using UnityEngine;
using Zenject;

namespace RTS.Game.FogOfWar
{
    public class HidingInFog : MonoBehaviour
        {
        [SerializeField, Range(0f, 1f)] private float threshold = 0.1f;

        private Camera cam;
        private MeshRenderer meshRenderer;

        private static Texture2D shadowTexture;
        private RenderTexture renderTexture;
        private static Rect rect;
        private static bool isDirty = true;

        [Inject]
        private void Construct([Inject(Id = "UnexploredAreasCam")] Camera cam,
                                MeshRenderer meshRenderer)
        {
            this.cam = cam;
            this.meshRenderer = meshRenderer;
        }

        private Color GetColorAtPosition()
        {
            if (!cam)
            {
                return Color.white;
            }

            renderTexture = cam.targetTexture;
            if (!renderTexture)
            {
                return cam.backgroundColor;
            }

            if (shadowTexture == null || renderTexture.width != rect.width || renderTexture.height != rect.height)
            {
                rect = new Rect(0, 0, renderTexture.width, renderTexture.height);
                shadowTexture = new Texture2D((int)rect.width, (int)rect.height, TextureFormat.RGB24, false);
            }

            if (isDirty)
            {
                RenderTexture.active = renderTexture;
                shadowTexture.ReadPixels(rect, 0, 0);
                RenderTexture.active = null;
                isDirty = false;
            }

            var pixel = cam.WorldToScreenPoint(transform.position);
            return shadowTexture.GetPixel((int)pixel.x, (int)pixel.y);
        }

        private void Update()
        {
            isDirty = true;
        }

        private void LateUpdate()
        {
            if (!meshRenderer)
            {
                this.enabled = false;
                return;
            }
            meshRenderer.enabled = GetColorAtPosition().grayscale >= threshold;
        }
    }
}