using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace RTS
{
    public static class Utils
    {
        public static int TERRAIN_LAYER_MASK = 1 << 31; 

        public static Vector3[] ScreenCornersToWorldPoints()
        {
            return ScreenCornersToWorld(Camera.main);
        }

        public static Vector3[] ScreenCornersToWorld(Camera cam)
        {
            Vector3[] corners = new Vector3[4];
            RaycastHit hit;
            for (int i = 0; i < 4; i++)
            {
                Ray ray = cam.ScreenPointToRay(new Vector2((i % 2) * Screen.width, (int)(i / 2) * Screen.height));
                if (Physics.Raycast(
                        ray,
                        out hit,
                        1000f
                    )) corners[i] = hit.point;
            }
            return corners;
        }

        public static Vector3 MiddleOfScreenPointToWorld()
        {
            return MiddleOfScreenPointToWorld(Camera.main);
        }

        public static Vector3 MiddleOfScreenPointToWorld(Camera cam)
        {
            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(0.5f * new Vector2(Screen.width, Screen.height));
            if (Physics.Raycast(ray, out hit, 1000f))
            {
                return hit.point;
            }
            return Vector3.zero;
        }

        public static Mesh CreateCircle(float radius, float quality)
        {
            Mesh mesh = new Mesh();
            float angleStep = 360.0f / (float)quality;
            List<Vector3> vertexList = new List<Vector3>();
            List<int> triangleList = new List<int>();
            Quaternion quaternion = Quaternion.Euler(0.0f, 0.0f, angleStep);
            vertexList.Add(new Vector3(0.0f, 0.0f, 0.0f));
            vertexList.Add(new Vector3(0.0f, radius / 5f, 0.0f));
            vertexList.Add(quaternion * vertexList[1]);

            triangleList.Add(0);
            triangleList.Add(1);
            triangleList.Add(2);
            for (int i = 0; i < quality - 1; i++)
            {
                triangleList.Add(0);
                triangleList.Add(vertexList.Count - 1);
                triangleList.Add(vertexList.Count);
                vertexList.Add(quaternion * vertexList[vertexList.Count - 1]);
            }
            mesh.vertices = vertexList.ToArray();
            mesh.triangles = triangleList.ToArray();
            return mesh;
        }

        public static float GetHeightOfTerrain(Vector3 point)
        {
            return Terrain.activeTerrain.SampleHeight(point);
        }
        
        public static bool IsMouseOverUI => EventSystem.current.IsPointerOverGameObject();

        public static Vector2 MousePos()
        {
            return Mouse.current.position.ReadValue();
        }
    }
}