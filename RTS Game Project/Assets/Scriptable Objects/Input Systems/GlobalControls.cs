// GENERATED AUTOMATICALLY FROM 'Assets/_Project/Scriptable Objects/Input System/GlobalControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @GlobalControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @GlobalControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""GlobalControls"",
    ""maps"": [
        {
            ""name"": ""Camera"",
            ""id"": ""34096eef-191f-4e4a-a7c5-3f39c74e7cea"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""0d4806e2-c9c7-4590-8bfb-0fa8ef1534e0"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Zoom"",
                    ""type"": ""PassThrough"",
                    ""id"": ""f916e781-ec64-409b-84cb-7fe19874da37"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""37677468-caa0-467f-b5a9-564028ba57eb"",
                    ""path"": ""2DVector(mode=1)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""02b5b9b7-96d9-4729-9521-adde2ba3d437"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""6e3f6438-c140-46ab-94d4-c7d8be4881b3"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""eea07ec6-9c43-4104-89b0-3bcf5de749b1"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""e09079b0-cf92-470f-ad09-c0bb0e9be554"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""ed60c61e-787f-4b78-8a85-a4e730679e67"",
                    ""path"": ""<Mouse>/scroll/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Zoom"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Game"",
            ""id"": ""5ef00642-8c44-4b01-80a5-ebf6aa2b6714"",
            ""actions"": [
                {
                    ""name"": ""Left Mouse Button Click"",
                    ""type"": ""Button"",
                    ""id"": ""6c13f5cf-acc5-441b-b5d5-da3c1da51067"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Right Mouse Button Click"",
                    ""type"": ""Button"",
                    ""id"": ""1cb15619-e320-459d-86ad-dd90c59688e3"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Left Shift Click"",
                    ""type"": ""Button"",
                    ""id"": ""fa6a7511-6383-4aa3-bd0e-e8b617d72033"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Left Alt Click"",
                    ""type"": ""Button"",
                    ""id"": ""01b44b0d-8008-45cc-8d9d-5c16a7ea9664"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Escape"",
                    ""type"": ""Button"",
                    ""id"": ""a4a82d62-49f6-44e5-9afb-8b69604e68cb"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""P"",
                    ""type"": ""Button"",
                    ""id"": ""8be9a5f8-2d42-491a-b29b-03158fa99013"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ShiftSelect"",
                    ""type"": ""Button"",
                    ""id"": ""ca0f3807-ed33-473c-a5f2-a70787c8430a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""2d1c596a-3fcc-40d2-956b-7de199282324"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Left Mouse Button Click"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c3203e74-cd73-4621-8ac6-1cba0d499311"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Left Shift Click"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e365be37-b2ec-41f4-9802-8a005ed9f932"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Right Mouse Button Click"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8aa3caae-0270-4648-b37f-029d3a376b57"",
                    ""path"": ""<Keyboard>/leftAlt"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Left Alt Click"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""91f38844-c2c9-42d4-a443-9865cfaed409"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Escape"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""816d6d6b-d915-4d40-bb1b-7b7b55209839"",
                    ""path"": ""<Keyboard>/p"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""P"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""ShiftSelect"",
                    ""id"": ""122ac0fe-1690-4fef-ba39-5959eda55693"",
                    ""path"": ""ButtonWithOneModifier"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ShiftSelect"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""modifier"",
                    ""id"": ""f2e29843-edf7-4601-8b24-c8eed7dd16b0"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ShiftSelect"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""button"",
                    ""id"": ""823191e2-e3ae-4702-9f14-e532ddf2a8e3"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ShiftSelect"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                }
            ]
        },
        {
            ""name"": ""Menu"",
            ""id"": ""735933ec-2089-4a5c-aaf3-7b197f93579a"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""f8a86a43-e0bf-4901-be2b-7dd62877a2c3"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Back"",
                    ""type"": ""Button"",
                    ""id"": ""e6fafe62-91f6-4277-8ffe-14d014e657fe"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Enter"",
                    ""type"": ""Button"",
                    ""id"": ""03e4606b-041e-4ad8-9537-958168dcc25d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""36fede81-087f-4df4-93dc-f58fa288f510"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""054fe34b-7d68-4cfc-a320-e4a2d2bed689"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""6073be50-7692-47ca-bb0f-a8ed130ebe0e"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""f77bb2d9-3316-4243-84e7-0fe18d55eb72"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""effcba22-0ba3-4cc3-bdb9-08d503eacd27"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""09e796db-254b-4da9-a2d3-48b6e5b23435"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""88fb081d-3d97-497d-81e5-9923720805c5"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""f988b6af-0afc-4635-b610-d696016d064a"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""3e7b81e2-365d-4221-9dff-2dbbe3ccaa87"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""e069787e-5b2d-4341-8a3a-f133897068e9"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""026047df-31d0-4bb8-adb0-993c724f0aa7"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Back"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fe664cf0-81a6-410c-96de-1eaaa6fd003a"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Enter"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Camera
        m_Camera = asset.FindActionMap("Camera", throwIfNotFound: true);
        m_Camera_Move = m_Camera.FindAction("Move", throwIfNotFound: true);
        m_Camera_Zoom = m_Camera.FindAction("Zoom", throwIfNotFound: true);
        // Game
        m_Game = asset.FindActionMap("Game", throwIfNotFound: true);
        m_Game_LeftMouseButtonClick = m_Game.FindAction("Left Mouse Button Click", throwIfNotFound: true);
        m_Game_RightMouseButtonClick = m_Game.FindAction("Right Mouse Button Click", throwIfNotFound: true);
        m_Game_LeftShiftClick = m_Game.FindAction("Left Shift Click", throwIfNotFound: true);
        m_Game_LeftAltClick = m_Game.FindAction("Left Alt Click", throwIfNotFound: true);
        m_Game_Escape = m_Game.FindAction("Escape", throwIfNotFound: true);
        m_Game_P = m_Game.FindAction("P", throwIfNotFound: true);
        m_Game_ShiftSelect = m_Game.FindAction("ShiftSelect", throwIfNotFound: true);
        // Menu
        m_Menu = asset.FindActionMap("Menu", throwIfNotFound: true);
        m_Menu_Move = m_Menu.FindAction("Move", throwIfNotFound: true);
        m_Menu_Back = m_Menu.FindAction("Back", throwIfNotFound: true);
        m_Menu_Enter = m_Menu.FindAction("Enter", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Camera
    private readonly InputActionMap m_Camera;
    private ICameraActions m_CameraActionsCallbackInterface;
    private readonly InputAction m_Camera_Move;
    private readonly InputAction m_Camera_Zoom;
    public struct CameraActions
    {
        private @GlobalControls m_Wrapper;
        public CameraActions(@GlobalControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_Camera_Move;
        public InputAction @Zoom => m_Wrapper.m_Camera_Zoom;
        public InputActionMap Get() { return m_Wrapper.m_Camera; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(CameraActions set) { return set.Get(); }
        public void SetCallbacks(ICameraActions instance)
        {
            if (m_Wrapper.m_CameraActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_CameraActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_CameraActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_CameraActionsCallbackInterface.OnMove;
                @Zoom.started -= m_Wrapper.m_CameraActionsCallbackInterface.OnZoom;
                @Zoom.performed -= m_Wrapper.m_CameraActionsCallbackInterface.OnZoom;
                @Zoom.canceled -= m_Wrapper.m_CameraActionsCallbackInterface.OnZoom;
            }
            m_Wrapper.m_CameraActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Zoom.started += instance.OnZoom;
                @Zoom.performed += instance.OnZoom;
                @Zoom.canceled += instance.OnZoom;
            }
        }
    }
    public CameraActions @Camera => new CameraActions(this);

    // Game
    private readonly InputActionMap m_Game;
    private IGameActions m_GameActionsCallbackInterface;
    private readonly InputAction m_Game_LeftMouseButtonClick;
    private readonly InputAction m_Game_RightMouseButtonClick;
    private readonly InputAction m_Game_LeftShiftClick;
    private readonly InputAction m_Game_LeftAltClick;
    private readonly InputAction m_Game_Escape;
    private readonly InputAction m_Game_P;
    private readonly InputAction m_Game_ShiftSelect;
    public struct GameActions
    {
        private @GlobalControls m_Wrapper;
        public GameActions(@GlobalControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @LeftMouseButtonClick => m_Wrapper.m_Game_LeftMouseButtonClick;
        public InputAction @RightMouseButtonClick => m_Wrapper.m_Game_RightMouseButtonClick;
        public InputAction @LeftShiftClick => m_Wrapper.m_Game_LeftShiftClick;
        public InputAction @LeftAltClick => m_Wrapper.m_Game_LeftAltClick;
        public InputAction @Escape => m_Wrapper.m_Game_Escape;
        public InputAction @P => m_Wrapper.m_Game_P;
        public InputAction @ShiftSelect => m_Wrapper.m_Game_ShiftSelect;
        public InputActionMap Get() { return m_Wrapper.m_Game; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(GameActions set) { return set.Get(); }
        public void SetCallbacks(IGameActions instance)
        {
            if (m_Wrapper.m_GameActionsCallbackInterface != null)
            {
                @LeftMouseButtonClick.started -= m_Wrapper.m_GameActionsCallbackInterface.OnLeftMouseButtonClick;
                @LeftMouseButtonClick.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnLeftMouseButtonClick;
                @LeftMouseButtonClick.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnLeftMouseButtonClick;
                @RightMouseButtonClick.started -= m_Wrapper.m_GameActionsCallbackInterface.OnRightMouseButtonClick;
                @RightMouseButtonClick.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnRightMouseButtonClick;
                @RightMouseButtonClick.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnRightMouseButtonClick;
                @LeftShiftClick.started -= m_Wrapper.m_GameActionsCallbackInterface.OnLeftShiftClick;
                @LeftShiftClick.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnLeftShiftClick;
                @LeftShiftClick.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnLeftShiftClick;
                @LeftAltClick.started -= m_Wrapper.m_GameActionsCallbackInterface.OnLeftAltClick;
                @LeftAltClick.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnLeftAltClick;
                @LeftAltClick.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnLeftAltClick;
                @Escape.started -= m_Wrapper.m_GameActionsCallbackInterface.OnEscape;
                @Escape.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnEscape;
                @Escape.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnEscape;
                @P.started -= m_Wrapper.m_GameActionsCallbackInterface.OnP;
                @P.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnP;
                @P.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnP;
                @ShiftSelect.started -= m_Wrapper.m_GameActionsCallbackInterface.OnShiftSelect;
                @ShiftSelect.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnShiftSelect;
                @ShiftSelect.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnShiftSelect;
            }
            m_Wrapper.m_GameActionsCallbackInterface = instance;
            if (instance != null)
            {
                @LeftMouseButtonClick.started += instance.OnLeftMouseButtonClick;
                @LeftMouseButtonClick.performed += instance.OnLeftMouseButtonClick;
                @LeftMouseButtonClick.canceled += instance.OnLeftMouseButtonClick;
                @RightMouseButtonClick.started += instance.OnRightMouseButtonClick;
                @RightMouseButtonClick.performed += instance.OnRightMouseButtonClick;
                @RightMouseButtonClick.canceled += instance.OnRightMouseButtonClick;
                @LeftShiftClick.started += instance.OnLeftShiftClick;
                @LeftShiftClick.performed += instance.OnLeftShiftClick;
                @LeftShiftClick.canceled += instance.OnLeftShiftClick;
                @LeftAltClick.started += instance.OnLeftAltClick;
                @LeftAltClick.performed += instance.OnLeftAltClick;
                @LeftAltClick.canceled += instance.OnLeftAltClick;
                @Escape.started += instance.OnEscape;
                @Escape.performed += instance.OnEscape;
                @Escape.canceled += instance.OnEscape;
                @P.started += instance.OnP;
                @P.performed += instance.OnP;
                @P.canceled += instance.OnP;
                @ShiftSelect.started += instance.OnShiftSelect;
                @ShiftSelect.performed += instance.OnShiftSelect;
                @ShiftSelect.canceled += instance.OnShiftSelect;
            }
        }
    }
    public GameActions @Game => new GameActions(this);

    // Menu
    private readonly InputActionMap m_Menu;
    private IMenuActions m_MenuActionsCallbackInterface;
    private readonly InputAction m_Menu_Move;
    private readonly InputAction m_Menu_Back;
    private readonly InputAction m_Menu_Enter;
    public struct MenuActions
    {
        private @GlobalControls m_Wrapper;
        public MenuActions(@GlobalControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_Menu_Move;
        public InputAction @Back => m_Wrapper.m_Menu_Back;
        public InputAction @Enter => m_Wrapper.m_Menu_Enter;
        public InputActionMap Get() { return m_Wrapper.m_Menu; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MenuActions set) { return set.Get(); }
        public void SetCallbacks(IMenuActions instance)
        {
            if (m_Wrapper.m_MenuActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnMove;
                @Back.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnBack;
                @Back.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnBack;
                @Back.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnBack;
                @Enter.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnEnter;
                @Enter.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnEnter;
                @Enter.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnEnter;
            }
            m_Wrapper.m_MenuActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Back.started += instance.OnBack;
                @Back.performed += instance.OnBack;
                @Back.canceled += instance.OnBack;
                @Enter.started += instance.OnEnter;
                @Enter.performed += instance.OnEnter;
                @Enter.canceled += instance.OnEnter;
            }
        }
    }
    public MenuActions @Menu => new MenuActions(this);
    public interface ICameraActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnZoom(InputAction.CallbackContext context);
    }
    public interface IGameActions
    {
        void OnLeftMouseButtonClick(InputAction.CallbackContext context);
        void OnRightMouseButtonClick(InputAction.CallbackContext context);
        void OnLeftShiftClick(InputAction.CallbackContext context);
        void OnLeftAltClick(InputAction.CallbackContext context);
        void OnEscape(InputAction.CallbackContext context);
        void OnP(InputAction.CallbackContext context);
        void OnShiftSelect(InputAction.CallbackContext context);
    }
    public interface IMenuActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnBack(InputAction.CallbackContext context);
        void OnEnter(InputAction.CallbackContext context);
    }
}
